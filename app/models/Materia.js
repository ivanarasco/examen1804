const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiaSchema = new Schema({
  // la estructura para crear los objetos
  codigo: {
    type: Number,
    unique: true,
    required: true
  },
  nombre: {
    type: String,
    maxLength: 45,
    required: true
  },
  curso: {
    type: String,
    required: true
  },
  horas: {
    type: Number,
    maxLength: 2
  },
  created: {
    type: Date,
    default: Date.now
  }
})

const Materia = mongoose.model('Materia', materiaSchema)
// guardamos el modelo

module.exports = Materia
