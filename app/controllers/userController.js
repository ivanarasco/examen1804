// const mongoose = require('mongoose')
const User = require('../models/User')
const servicejwt = require('../services/servicejwt')

function login (req, res) {
  User.findOne({ nombre: req.body.nombre.toLowerCase() }, function (err, user) {
    return res.send({ token: servicejwt.createToken(user) })
    // comprobamos si el usuario con ese nombre existe y, en caso afirmativo, devuelve el token.
  })
}

module.exports = { login }
