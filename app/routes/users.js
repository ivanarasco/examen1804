const express = require('express')

const router = express.Router()

const userController = require('../controllers/userController.js')

// iniciar sesión...
router.post('/login', (req, res) => {
  // index
  userController.login(req, res)
})

module.exports = router
