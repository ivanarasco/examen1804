const express = require('express')
const auth = require('../middlewares/auth')
const router = express.Router()

const materiaController = require('../controllers/materiaController.js')
// las rutas que pongamos nos llevarán a los métodos del controlador

router.get('/privada', auth, materiaController.indexPrivada)
// solo para el método index de este controlador, aplicando el método auth

router.get('/', (req, res) => {
  // index
  materiaController.index(res)
})

router.get('/privada', (req, res) => {
  // index de materias con token
  materiaController.indexPrivada(res)
})

router.get('/:id', (req, res) => {
  // show con el id
  materiaController.show(req, res)
})

router.post('/', (req, res) => {
  // creación
  materiaController.create(req, res)
})

router.delete('/:id', (req, res) => {
  // borrado
  materiaController.remove(req, res)
})

router.put('/:id', (req, res) => {
  // actualización
  materiaController.update(req, res)
})

module.exports = router
